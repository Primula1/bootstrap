#pragma once
#include "pch.h"
#include <iostream>
#include <random>

using std::cout;
using std::endl;
using std::cin;

template <typename T>
class DynamicArray 
{
public:
	DynamicArray(unsigned int initialAllocated = 10) // creating array to the size allocated through input, also changes the amount of elements used to 0 // using an = in constructor paramaters to allocate a variables values makes the compiler treat the variable as if it is constructing it, so still remains as the default constructor
	{

		allocatedElements = initialAllocated;
		data = new T[allocatedElements];

		//data = new T;

		usedElements = 0;
	}


	DynamicArray(const DynamicArray& other) // copy constructor
	{
		// Use the assignment operator to allocate/copy the array.
		// Saves us having to write the same code again
		operator=(other);
	}


	DynamicArray& operator=(const DynamicArray& other) 	// Assignment operator
	{
		if (data != nullptr) 
		{
			delete[] data;
		}

		allocatedElements = other.allocatedElements; // update the amount of allocated elements 
		data = new T[allocatedElements];

		usedElements = other.usedElements; // Update the number of elements currently in use on our new array (from the original array)

		//// Then copy the data from the other DynamicArray to our new DynamicArray
		for (unsigned int i = 0; i < usedElements; i++) 
		{
			data[i] = other.data[i];
		}
		return *this;
	}



	~DynamicArray() // DECONSTRUCTOR
	{
		if (data != nullptr)
		{
			delete[] data;
			data = nullptr;
		}
	}


	DynamicArray& addToArrayEnd(T newElement) {

		if (usedElements >= allocatedElements)
		{
			resize();
		}

		data[usedElements] = newElement; // change variable at elements location with that arrays data
		usedElements += 1;

		return *this;
	}

	DynamicArray& print() {
		for (unsigned int i = 0; i < usedElements; i++)
		{
			cout << data[i] << ", ";
		}
		cout << "\n\n";
		return *this;
	}


	DynamicArray& removeFromEnd() {

		if (usedElements > 0) {
			usedElements -= 1;
		}

		return *this; // calls copy constructor when working with copies
	}


	// class names are return types, containing all the newly created data inside that class    e.g. for DynamicArray it will either copy everything from its protected section which is all initialised data or passthrough reference to them if reference starts with reference to '&' and returns with *this
	DynamicArray& insertElement(unsigned int section, T newElement/*, array array**/) { // shift from usedElements location going down to shift everything right // first check if container is big enough, else expand the array
		
		bool overwrite = true;

		if (usedElements >= allocatedElements)
		{
			resize();			
		}
			if (overwrite == true)
			{
				for (unsigned int i = usedElements; i != section; i--)
				{
					data[i] = data[(i - 1)];
				}
				usedElements++;
			}
	
		data[section] = newElement; 
		return *this;
	}


	void randomAssign() {
		
		for (unsigned int i = 0; i < allocatedElements; i++) {
			data[i] = rand() % 20;
			usedElements++;
		}
	}


	DynamicArray& resize() {
		bool rightSize_b = false;

		while (rightSize_b == false) {
			cout << "Array must be resized, Please specify how many spots shall be allocated between 1 - 200: ";
			cin >> size;
			
			if (size < 0 || size > 200 || !cin) {
				cin.clear();
				cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); 
				cout << "Invalid amount, please try again \n\n";
			}
			else {
				rightSize_b = true;
			}
		}
	
		allocatedElements += size;
		newData = new T[allocatedElements];

		//copy -- look into memcpy()
			for (unsigned int i = 0; i < usedElements; i++)
			{
				newData[i] = data[i];
			}
		delete[] data;
		data = newData;

		return *this; // return this is used to pass through an object which could be many things e.g. a class or array containing multiple data types, and the pointer is used to ________
	}		


	void clearInput() {
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
	}


	void removeFromMiddle(int section) {		
		bool ordered = true;
		char input;

		cout << "Would you like to preserve array order or not? Y/N \n";
		cin >> input;

		input == 'y' ? ordered = true : ordered = false;
		clearInput();

		if (usedElements >= allocatedElements)
			{
				resize();
			}
		
		if (ordered == false) {
			data[section] = data[usedElements - 1]; // overwrite sections data with the end values data
		}
		else 
		{
			for (unsigned int i = section; i != usedElements; i++)
			{
				data[i] = data[(i + 1)];
			}
		}
		usedElements--;

	}

	DynamicArray& clearArray() {
	
		usedElements = 0;
		cout << "Array has been cleared\n";
		return *this;
	}


protected:
	T * newData;
	T * data; // T / or data becomes a pointer to one 'data' when it is created, like in battle arena which holds data for that individual data when created
	unsigned int usedElements;
	unsigned int allocatedElements;
	unsigned int size = 0;
};




///////// NOTES

// other is the first array that will be passed through in order to create a new array, copy over the originals content, then delete it.

// 